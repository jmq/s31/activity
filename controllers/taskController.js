/*
* Description: Contains the functions and business logic of our Express JS app
*/

const Task = require('../models/task') //Import task package

// Controller function for getting all the tasks
// export it immediately 
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Controller function for creating task
module.exports.createTask = (requestBody) => {
	const newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})


// Controller function for creating task
module.exports.createStrictTask = (requestBody) => {
	Task.findOne({name: requestBody.name}, (err, result) => {
		if(result != null && result.name === requestBody.name) {
			return `${requestBody.name} already exists.`
		} else {
			const newTask = new Task({
				name: requestBody.name
			})
			return newTask.save().then((task, error) => {
				if(error) {
					console.log(error)
					return false
				} else {
					return task
				}
			})
		}
	})
}




// Controller for deleting a task
module.exports.deleteTask = (taskID) => {
	return Task.findByIdAndRemove(taskID).then((removedTask, err) => {
		if(err) { 
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

// Controller for updating a task
module.exports.updateTask = (taskID, newContent) => {
	//return Task.findByIdAndUpdate(task)
	return Task.findById(taskID).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} 
		result.name = newContent.name
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})		
	})
}

// Controller for updating a specific task
module.exports.completeTask = (taskID) => {
	//return Task.findByIdAndUpdate(task)
	return Task.findById(taskID).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} 
		result.status = 'complete'
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
				return false
			} else {
				return updatedTask
			}
		})		
	})
}


module.exports.getSpecificTask = (taskID) => {
	return Task.find({_id: taskID}).then(result => {
		return result
	})
}
