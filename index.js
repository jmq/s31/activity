const mongoose = require('mongoose')
const express = require('express')
const taskRoute = require('./routes/taskRoute')

// Server Setup
const app = express()
const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect("mongodb+srv://admin:admin123@course-booking.m222n.mongodb.net/B157_to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology: true
});

let db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection error.'))
db.once('open', () => console.log("We're connected to the cloud database"))


//Add the task route
app.use('/tasks', taskRoute)



app.listen(port, () => console.log(`Now listening to port ${port}`))