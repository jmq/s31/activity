/*
* Description: Contains all of the endpoints of the application
*/

const express = require('express')
const app = express()
const router = express.Router()	//Allows access to HTTP method middlewares to create routes

const taskController = require('../controllers/taskController') //import taskController package

// Route t get all the tasks
router.get('/', (req, res) => {
	taskController.getAllTasks().then(resultFromController => {
		res.send(resultFromController)
	})
})

// Route to create a task
// .then sends the result to client or postman
router.post('/', (req, res) => {
	taskController.createTask(req.body).then(resultFromController => {
		res.send(resultFromController)
	})
})


// Route to Delete
router.delete('/:id', (req, res) => {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(`Task ${resultFromController.name} has been deleted.`)
	})
})


// Route to update
router.put('/:id', (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => {
		res.send(`${req.params.id} has been updated ${resultFromController.status}.`)
	})
})


// Route to get specific task
router.get('/:id', (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Route to update specific task
router.put('/:id/complete', (req, res) => {
	taskController.completeTask(req.params.id).then(resultFromController => {
		res.send(resultFromController)
	})
})


module.exports = router // used to export the router object so it can be used globally